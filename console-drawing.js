
module.exports = function() {
  var _canvas,
      COLUMN_BOUNDARY,
      ROW_BOUNDARY;

  return {
    createCanvas: createCanvas,
    createLine: createLine,
    createRectangle: createRectangle,
    bucketFill: bucketFill,
    validatePrompt: validatePrompt
  };

  /**
   * @function createCanvas
   */
  function createCanvas(width, height) {
    _canvas = new Array(parseInt(height) + 2);
    _canvas.fill(' ');
    _canvas.forEach(function(row, index) {
      _canvas[index] = new Array(parseInt(width) + 2);
      _canvas[index].fill(' ');
    })

    ROW_BOUNDARY = _canvas.length - 1;
    COLUMN_BOUNDARY = _canvas[0].length - 1

    _createXBorder(0, 0, COLUMN_BOUNDARY, ROW_BOUNDARY, '-');
    _createYBorder(0, 1, COLUMN_BOUNDARY, ROW_BOUNDARY, '|');
    _printDrawing();
  }

  /**
   * @function createLine
   * @description This function can:
   * 1. Create a horizontal line from left to right
   * 2. Create a horizontal line from right to left
   * 3. Create a vertical line from top to bottom
   * 4. Create a vertical line from bottom to top
   */
  function createLine(x1, y1, x2, y2) {
    var xStart = Math.min(x1, x2),
        xEnd = Math.max(x1, x2),
        yStart = Math.min(y1, y2),
        yEnd = Math.max(y1, y2);

    var isValidLine = xStart > 0 &&
                      xEnd > 0 &&
                      yStart > 0 &&
                      yEnd > 0 &&
                      xEnd < COLUMN_BOUNDARY &&
                      yEnd < ROW_BOUNDARY;

    if (_canvas && isValidLine) {
      // Create Vertical Line
      if (x1 === x2) {
        for (var y = yStart; y <= yEnd; y++) {
          _canvas[y][x1] = 'x';
        }
      }
      // Create Horizontal Line
      else if (y1 === y2) {
        for (var x = xStart; x <= xEnd; x++) {
          _canvas[y1][x] = 'x';
        }
      }
      _printDrawing();
    }
    else {
      var errorMessage = !_canvas ?
          'You must create a canvas first\n' :
          'Invalid line coordinates.\n';

      return console.log(errorMessage);
    }
  }

  /**
   * @function createRectangle
   * @description Create a rectangle using top left to bottom right coordinates.
   */
  function createRectangle(x1, y1, x2, y2) {
    var isValidRectangle = x1 > 0 &&
                          y1 > 0 &&
                          x2 > 0 &&
                          y2 > 0 &&
                          x1 < x2 &&
                          y1 < y2 &&
                          x2 < COLUMN_BOUNDARY &&
                          y2 < ROW_BOUNDARY;

    if (_canvas && isValidRectangle) {
      _createXBorder(x1, y1, x2, y2, 'x');
      _createYBorder(x1, y1, x2, y2, 'x');
      _printDrawing();
    }
    else {
      var errorMessage = !_canvas ?
          'You must create a canvas first\n' :
          'Invalid rectangle coordinates.\n';

      return console.log(errorMessage);
    }
  }

  function bucketFill(x1, y1, color) {
		// Incomplete. I underestimated how long this would take.
		console.log('Sorry, this doesn\'t work yet.');
  }

  function validatePrompt(text) {
    var params = text.toString()
      .toUpperCase()
      .replace(/(\n|\r)/gm,"")
      .trim()
      .split(' ');

    var COMMAND_KEY = params[0];

    var isValidKey = ['C', 'L', 'R', 'B', 'Q'].indexOf(COMMAND_KEY !== -1);
    var isValidParamsLength;

    var options = [
      {
        'key': 'C',
        'length': 2
      },
      {
        'key': 'L',
        'length': 4
      },
      {
        'key': 'R',
        'length': 4
      },
      {
        'key': 'B',
        'length': 3
      },
      {
        'key': 'Q',
        'length': 0
      }
    ].map(function(obj) {
      if (obj.key === COMMAND_KEY) {
        isValidParamsLength = obj.length === params.length - 1 ? true : false;
      }
    });

    if (isValidKey && isValidParamsLength) {
      try {
        switch (COMMAND_KEY) {
          case 'C':
            createCanvas(params[1], params[2]);
            break;
          case 'L':
            createLine(params[1], params[2], params[3], params[4]);
            break;
          case 'R':
            createRectangle(params[1], params[2], params[3], params[4]);
            break;
          case 'B':
            bucketFill(params[1], params[2], params[3]);
            break;
          case 'Q':
            quit();
            break;
        }
      }
      catch(error) {
        console.log('Oops! Invalid parameters.\n');
      }
    }
    else {
      console.log('Oops! Invalid command.\n');
    }
  }

  function _createXBorder(x1, y1, x2, y2, character) {
    for (var x = x1; x <= x2; x++) {
      _canvas[y1][x] = character;
      _canvas[y2][x] = character;
    }
  }

  function _createYBorder(x1, y1, x2, y2, character) {
    for (var y = y1; y < y2; y++) {
      _canvas[y][x1] = character;
      _canvas[y][x2] = character;
    }
  }

  function _printDrawing() {
    _canvas.forEach(function(row, index) {
      console.log(_canvas[index].join(''));
    })
  }
}();
