## Setup Instructions
```
$ npm install
```

To run the program:
```
$ node index
```

To run unit tests:
```
$ npm test
```
