'use strict';

var readline = require('readline');
var draw = require('./console-drawing.js');

var rl = readline.createInterface(process.stdin, process.stdout);

rl.setPrompt('\rEnter Command > ');
rl.prompt();
rl.on('line', function (text) {
  draw.validatePrompt(text);
  rl.prompt();
}).on('close', quit);

function quit() {
  console.log('\nExiting.')
  process.exit(0);
}
