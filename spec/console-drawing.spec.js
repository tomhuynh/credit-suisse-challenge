var draw = require('../console-drawing.js');
var sinon = require('sinon');


describe('Console Drawing', function () {
  var spy = sinon.spy(console, 'log');

  afterEach(function() {
    spy.reset();
  });

  describe('validatePrompt', function() {
    it('should remove miscellaneous spaces and work', function() {
      draw.validatePrompt(' C 10 5 ')
    })

    it('should show a log message with incorrect parameters', function() {
      draw.validatePrompt('C 10 5 5 5 4');
      expect(spy.args[0][0]).toEqual('Oops! Invalid command.\n');
    })
  })

  describe('createCanvas', function() {
    it('should create the correct border length', function() {
      draw.createCanvas(20, 4);
      var borderString = spy.args[0][0];
      expect(borderString.length).toEqual(22);
    })
  })


  describe('createLine', function() {
    it('should be able to create a horizontal line from right to left' , function() {
      draw.createCanvas(20, 4);
      draw.createLine(6,2,1,2,);
    })
  })

  describe('createRectangle', function() {
    it('should be able to create a valid rectangle' , function() {
      draw.createCanvas(20, 4);
      draw.validatePrompt('R 14 1 18 3')
    })
  })
});
